var express = require('express');
var router = express.Router();
var mongodb = require('../db');
var url = require('url');

router.get('/', function(req, res, next) {
  mongodb.open(function (err, db) {
    if (err) {
      return;
    }

    db.collection('game', function (err, collection) {
      if (err) {
        mongodb.close();
        return;
      }

      collection.findOne({
      }, function (err, ret) {
        mongodb.close();
        if (err) {
          return;
        }
        res.render('index', { title: 'Express' });
      });
    });
  });
});

router.get('/api/gamelist', function(req, res, next) {
  var query = url.parse(req.url, true).query;

  mongodb.open(function (err, db) {
    if (err) {
      return;
    }

    db.collection('game', function (err, collection) {
      if (err) {
        mongodb.close();
        return;
      }

      collection.find({}, {
        skip: (query.page - 1) * 10,
        limit: 10
      }).toArray(function (err, docs) {
        if (err) {
          mongodb.close();
          return;
        }
        res.json(docs);
      });
    });
  });
});

router.post('/api/comment', function(req, res){
  mongodb.open(function (err, db) {
    if (err) {
      return;
    }

    db.collection('comment', function (err, collection) {
      if (err) {
        mongodb.close();
        return;
      }

      collection.insert(req.body, {
        safe: true,
      }, function (err, ret) {
        mongodb.close();
        if (err) {
          return;
        }
      });
    });
  });
  res.sendStatus(200);
});

router.post('/api/game/insert', function(req, res){
  mongodb.open(function (err, db) {
    if (err) {
      return;
    }

    db.collection('game', function (err, collection) {
      if (err) {
        mongodb.close();
        return;
      }

      collection.insert(req.body, {
        safe: true,
      }, function (err, ret) {
        mongodb.close();
        if (err) {
          return;
        }
      });
    });
  });
  res.sendStatus(200);
});

module.exports = router;
